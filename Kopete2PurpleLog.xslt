<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:common="http://exslt.org/common" xmlns:date="http://exslt.org/dates-and-times" xmlns:func="http://exslt.org/functions"  xmlns:local="http://example.org" extension-element-prefixes="common func">
        <xsl:output method="text" />

        <!-- Parses that strange "time" attribute with combined day and no leading zeroes and creates a reasonable time value out of it -->
        <func:function name="local:formatTime">
                <xsl:param name="timeField" />
                <xsl:variable name="time" select="substring-after($timeField, ' ')" />
                <func:result><xsl:value-of select="format-number(substring-before($time, ':'), '00')" />:<xsl:value-of select="format-number(substring-before(substring-after($time, ':'), ':'), '00')" />:<xsl:value-of select="format-number(substring-after(substring-after($time, ':'), ':'), '00')" /></func:result>
        </func:function>

        <xsl:template match="/kopete-history">
                <xsl:call-template name="processEntries" />
        </xsl:template>

        <xsl:template name="processEntries">
                <xsl:param name="day" select="1" />
                <xsl:variable name="startDate">
                        <xsl:value-of select="format-number(head/date/@year, '0000')" />-<xsl:value-of select="format-number(head/date/@month, '00')" />-<xsl:value-of select="format-number($day, '00')" />
                </xsl:variable>
                <xsl:variable name="heading">
                        <xsl:text>Conversation with </xsl:text><xsl:value-of select="head/contact[not(@type='myself')]/@contactId" /> at <xsl:value-of select="substring(date:day-abbreviation($startDate), 1, 2)" /><xsl:text> </xsl:text><xsl:value-of select="format-number(substring-before(msg/@time[starts-with(., concat($day, ' '))], ' '), '00')" /><xsl:text> </xsl:text><xsl:value-of select="date:month-abbreviation($startDate)" /><xsl:text> </xsl:text><xsl:value-of select="format-number(head/date/@year, '0000')" /><xsl:text> </xsl:text><xsl:value-of select="local:formatTime(msg/@time[starts-with(., concat($day, ' '))])" /> on <xsl:value-of select="head/contact[@type='myself']/@contactId" /><xsl:text>/ (jabber)</xsl:text>
                </xsl:variable>
                <xsl:if test="$day &lt; 32">
                        <xsl:if test="count(msg/@time[starts-with(., concat($day, ' '))]) &gt; 0">
                                <xsl:variable name="outfile">
                                        <xsl:value-of select="head/contact[@type='myself']/@contactId" />/<xsl:value-of select="head/contact[not(@type='myself')]/@contactId" />/<xsl:value-of select="$startDate" /><xsl:text> </xsl:text><xsl:value-of select="local:formatTime(msg/@time[starts-with(., concat($day, ' '))])" />
                                </xsl:variable>
                                <common:document href="{$outfile}" method="text">
                                        <xsl:text>&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"&gt;&lt;html&gt;&lt;head&gt;&lt;meta http-equiv="Content-Type" content="text/html; charset=utf-8"&gt;&lt;title&gt;</xsl:text><xsl:value-of select="$heading" />&lt;/title&gt;&lt;/head&gt;&lt;body&gt;&lt;h1&gt;<xsl:value-of select="$heading" /><xsl:text>&lt;/h1&gt;&lt;p&gt;&#xa;</xsl:text>
                                        <xsl:for-each select="msg[@time[starts-with(., concat($day, ' '))]]">
                                                <xsl:text>&lt;span style="color: </xsl:text><xsl:if test="@in = 1">#A82F2F</xsl:if><xsl:if test="@in = 0">#16569E</xsl:if><xsl:text>"&gt;&lt;span style="font-size: smaller">(</xsl:text><xsl:value-of select="local:formatTime(@time)" /><xsl:text>)&lt;/span&gt; &lt;b&gt;</xsl:text><xsl:value-of select="@nick" /><xsl:text>:&lt;/b&gt;&lt;/span&gt; </xsl:text><xsl:value-of select="." /><xsl:text>&lt;br&gt;&#xa;</xsl:text>
                                        </xsl:for-each>
                                        <xsl:text>&lt;/p&gt;&#xa;</xsl:text>
                                        <xsl:text>&lt;/body&gt;&#xa;</xsl:text>
                                        <xsl:text>&lt;/html&gt;&#xa;</xsl:text>
                                </common:document>
                        </xsl:if>
                        <xsl:call-template name="processEntries">
                                <xsl:with-param name="day" select="$day + 1" />
                        </xsl:call-template>
                </xsl:if>
        </xsl:template>

        <xsl:template match="text()" />
</xsl:stylesheet>
