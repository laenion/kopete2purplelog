# Kopete2PurpleLog

Want to migrate away from Kopete, but don't want to loose all your chat history? This script will transform Kopete logs into the libpurple log file format, used by several messenger applications such as Pidgin.

## System requirements
The script uses `xsltproc` to convert Kopete's log files. On most distributions this is installed by default already.

## Paths
Typical paths of the log file directories:

* Kopete (KDE4): _~/.kde/share/apps/kopete/logs/_
* Kopete (KDE5): _~/.local/share/kopete/logs/_
* libpurple: _~/.purple/logs/_

## Supported protocols
Currently only **Jabber (XMPP)** and **ICQ** are supported, but support for additional protocols should be trivial - it's just that I haven't tested them.

If you want to add an additional protocol please check the Kopete log directory: It will contain a separate directory for each protocol (which should be something like *<Protocolname>Protocol*); then add an account with the same protocol in your libpurple based messenger and check the name of the directory in libpurple's log directory. With those two directory names just add that pair as a new `elif` query into the Bash script. Ideally you should also notify me so that I can add that protocol and others don't have to do the same :-)

## Usage
`./Kopete2PurpleLog.sh <path to Kopete logs> <path to libpurple logs>`, e.g. `./Kopete2PurpleLog.sh ~/.kde/share/apps/kopete/logs/ ~/.purple/logs/`.

## Technical details
Why using the HTML log file format over of the text format? Just in case one should want to migrate to another messenger with a different log file format, then the logs will have to be migrated again. Unfortunately the text file output leaves soom room for interpretation, so a conversion would potentially introduce inconsistencies. The HTML file output on the other hand in unambiguous.
