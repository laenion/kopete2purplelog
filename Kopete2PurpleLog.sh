#!/bin/bash -e

selfDir="$(dirname "$(realpath "$0")")"
echo $selfDir

if [ -z "$2" ]; then
	echo "Usage: Kopete2PurpleLog <Path to Kopete log directory> <output directory>"
	exit 1
fi
tmpdir="$(mktemp -d)"
outputPath="$(realpath "$2")"
sourcePath="$(realpath "$1")"
cd "${sourcePath}"

for source in *; do
	if [ "${source}" == "JabberProtocol" ]; then
		protocol="jabber"
	elif [ "${source}" == "ICQProtocol" ]; then
		protocol="icq"
	else
		continue
	fi

	pushd "${source}"
	for account in *; do
		pushd "${account}"
		for file in *.xml; do
			infile="$(readlink -f "${file}")"
			workingdir="${tmpdir}/${protocol}"
			mkdir -p "${workingdir}"
			pushd "${workingdir}"
			xsltproc "${selfDir}/Kopete2PurpleLog.xslt" "${infile}"
			popd
		done
		popd
	done
	popd
done

# Add timezone to filename
pushd ${tmpdir}
for file in */*/*/*; do
	base="$(basename "${file}")"
	dir="$(dirname "${file}")"
	mv "${file}" "$dir/$(date -d"${base}" +%F.%H%M%S%z%Z).html"
done
popd

cp -r "${tmpdir}"/* "${outputPath}"
rm -r "${tmpdir}"
